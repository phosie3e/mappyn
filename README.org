* mappyn, The Enumerator Cat Fantastic!

We all love to enumerate, for work or for pleasure, for wearing all nature of
hats, be they white, grey, or black.  We all get disinterested in the challenge
of memorizing obscure command line parameters, but that too gets old.

mappyn will attempt to solve that problem by mapping arguments and parameters of
different command line tools, starting with only nmap, advise you how to craft a
good set of arguments, and save it to a JSON file.  Then share with your friends
of all different hat varieties for fun happy 'numeration domination.

Also, for dealing with your more seasoned grey beards, it will also attempt to
reverse the commands from a previously extant NMAP xml report and convert into
this tool's JSON file.
* What's Next? (I Hope ...)
- [[https://github.com/zmap/zmap][zmap]]
- [[https://github.com/robertdavidgraham/masscan][masscan]]
* What's the Name About?
Clearly, you are not a golfer.  Just like on the tin in the header ...

#+BEGIN_SRC python
print("{} + {} = {}".format('python'[0:1], 'nmap'[:], "mappyn, totally rad Python enumeration wrapper!"))
#+END_SRC
* Questions? Comments? Thoughts?
- [[https://gitlab.com/phosie3e/mappyn][The repo for this code on Gitlab (preferred)]]
- [[https://github.com/xee5ch/mappyn][The repo for this code on Github (whatever, do what you must)]]
